﻿namespace Bab5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnload = new System.Windows.Forms.Button();
            this.btngrayscale = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbrightness = new System.Windows.Forms.TextBox();
            this.txtcontrast = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnbrightness = new System.Windows.Forms.Button();
            this.btncontrast = new System.Windows.Forms.Button();
            this.btninvers = new System.Windows.Forms.Button();
            this.btnautolevel = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnhistogram = new System.Windows.Forms.Button();
            this.btncdf = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnload
            // 
            this.btnload.Location = new System.Drawing.Point(21, 23);
            this.btnload.Name = "btnload";
            this.btnload.Size = new System.Drawing.Size(95, 33);
            this.btnload.TabIndex = 0;
            this.btnload.Text = "LOAD";
            this.btnload.UseVisualStyleBackColor = true;
            this.btnload.Click += new System.EventHandler(this.btnload_Click);
            // 
            // btngrayscale
            // 
            this.btngrayscale.Location = new System.Drawing.Point(122, 23);
            this.btngrayscale.Name = "btngrayscale";
            this.btngrayscale.Size = new System.Drawing.Size(93, 33);
            this.btngrayscale.TabIndex = 1;
            this.btngrayscale.Text = "GrayScale";
            this.btngrayscale.UseVisualStyleBackColor = true;
            this.btngrayscale.Click += new System.EventHandler(this.btngrayscale_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(243, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Brightness";
            // 
            // txtbrightness
            // 
            this.txtbrightness.Location = new System.Drawing.Point(305, 32);
            this.txtbrightness.Name = "txtbrightness";
            this.txtbrightness.Size = new System.Drawing.Size(33, 20);
            this.txtbrightness.TabIndex = 3;
            // 
            // txtcontrast
            // 
            this.txtcontrast.Location = new System.Drawing.Point(423, 32);
            this.txtcontrast.Name = "txtcontrast";
            this.txtcontrast.Size = new System.Drawing.Size(27, 20);
            this.txtcontrast.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(371, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Contrast";
            // 
            // btnbrightness
            // 
            this.btnbrightness.Location = new System.Drawing.Point(246, 58);
            this.btnbrightness.Name = "btnbrightness";
            this.btnbrightness.Size = new System.Drawing.Size(92, 32);
            this.btnbrightness.TabIndex = 6;
            this.btnbrightness.Text = "Brightness";
            this.btnbrightness.UseVisualStyleBackColor = true;
            this.btnbrightness.Click += new System.EventHandler(this.btnbrightness_Click);
            // 
            // btncontrast
            // 
            this.btncontrast.Location = new System.Drawing.Point(374, 58);
            this.btncontrast.Name = "btncontrast";
            this.btncontrast.Size = new System.Drawing.Size(93, 32);
            this.btncontrast.TabIndex = 7;
            this.btncontrast.Text = "Contrast";
            this.btncontrast.UseVisualStyleBackColor = true;
            this.btncontrast.Click += new System.EventHandler(this.btncontrast_Click);
            // 
            // btninvers
            // 
            this.btninvers.Location = new System.Drawing.Point(471, 19);
            this.btninvers.Name = "btninvers";
            this.btninvers.Size = new System.Drawing.Size(95, 33);
            this.btninvers.TabIndex = 8;
            this.btninvers.Text = "Invers";
            this.btninvers.UseVisualStyleBackColor = true;
            this.btninvers.Click += new System.EventHandler(this.btninvers_Click);
            // 
            // btnautolevel
            // 
            this.btnautolevel.Location = new System.Drawing.Point(578, 19);
            this.btnautolevel.Name = "btnautolevel";
            this.btnautolevel.Size = new System.Drawing.Size(92, 33);
            this.btnautolevel.TabIndex = 9;
            this.btnautolevel.Text = "Auto Level";
            this.btnautolevel.UseVisualStyleBackColor = true;
            this.btnautolevel.Click += new System.EventHandler(this.btnautolevel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(21, 112);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 205);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(344, 112);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 205);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnhistogram
            // 
            this.btnhistogram.Location = new System.Drawing.Point(86, 333);
            this.btnhistogram.Name = "btnhistogram";
            this.btnhistogram.Size = new System.Drawing.Size(111, 37);
            this.btnhistogram.TabIndex = 12;
            this.btnhistogram.Text = "Histogram Awal";
            this.btnhistogram.UseVisualStyleBackColor = true;
            this.btnhistogram.Click += new System.EventHandler(this.btnhistogram_Click);
            // 
            // btncdf
            // 
            this.btncdf.Location = new System.Drawing.Point(393, 333);
            this.btncdf.Name = "btncdf";
            this.btncdf.Size = new System.Drawing.Size(101, 37);
            this.btncdf.TabIndex = 13;
            this.btncdf.Text = "Histogram Akhir";
            this.btncdf.UseVisualStyleBackColor = true;
            this.btncdf.Click += new System.EventHandler(this.btncdf_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 376);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(326, 278);
            this.chart1.TabIndex = 14;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(344, 376);
            this.chart2.Name = "chart2";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart2.Series.Add(series2);
            this.chart2.Size = new System.Drawing.Size(326, 278);
            this.chart2.TabIndex = 15;
            this.chart2.Text = "chart2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Menurunkan   Contrast";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 674);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btncdf);
            this.Controls.Add(this.btnhistogram);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnautolevel);
            this.Controls.Add(this.btninvers);
            this.Controls.Add(this.btncontrast);
            this.Controls.Add(this.btnbrightness);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcontrast);
            this.Controls.Add(this.txtbrightness);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btngrayscale);
            this.Controls.Add(this.btnload);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnload;
        private System.Windows.Forms.Button btngrayscale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbrightness;
        private System.Windows.Forms.TextBox txtcontrast;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnbrightness;
        private System.Windows.Forms.Button btncontrast;
        private System.Windows.Forms.Button btninvers;
        private System.Windows.Forms.Button btnautolevel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnhistogram;
        private System.Windows.Forms.Button btncdf;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label label3;
    }
}

